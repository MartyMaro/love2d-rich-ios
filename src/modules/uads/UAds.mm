/**
 * Created by Martin Braun (modiX) for love2d (copied and modified from AdMob port by bio1712)
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

// LOVE
#include "common/config.h"
#include "UAds.h"
#include "UAdsDelegate.h"
#include "config_UAds.h"

#if defined(LOVE_MACOSX)
#include <CoreServices/CoreServices.h>
#elif defined(LOVE_IOS)
#include "common/ios.h"
#elif defined(LOVE_LINUX) || defined(LOVE_ANDROID)
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>
#elif defined(LOVE_WINDOWS)
#include "common/utf8.h"
#include <shlobj.h>
#include <shellapi.h>
#pragma comment(lib, "shell32.lib")
#endif
#if defined(LOVE_ANDROID)
#include "common/android.h"
#elif defined(LOVE_LINUX)
#include <spawn.h>
#endif

// SDL
#include "sdl/UAds.h"
//#include <SDL_clipboard.h>
//#include <SDL_cpuinfo.h>

// ObjC
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>
#import "UnityAds/UnityAds.h"


namespace love
{
	namespace uads
	{
		UAdsDelegate *callbackDelegate;
		
		UAds::UAds()
		{
			callbackDelegate = [[UAdsDelegate alloc] init];
			[callbackDelegate initProperties];
			[UnityAds initialize:[NSString stringWithUTF8String:UADS_GAME_ID] delegate:callbackDelegate testMode:UADS_TEST_MODE];
		}
		
		void UAds::test() const
		{
			printf("UADS_TEST\n");
		}
		
		bool UAds::isReady(const char *placementId)
		{
			return [UnityAds isReady:[NSString stringWithUTF8String:placementId]];
		}
		
		void UAds::show(const char *placementId)
		{
			if(isReady(placementId)) {
				[UnityAds show:getRootViewController() placementId:[NSString stringWithUTF8String:placementId]];
			}
		}
		
		std::string UAds::getLatestLoadPlacementId()
		{
			return [callbackDelegate.loadPlacementId UTF8String];
		}
		
		std::string UAds::getLatestStartPlacementId()
		{
			return [callbackDelegate.startPlacementId UTF8String];
		}
		
		std::string UAds::getLatestFinishPlacementId()
		{
			return [callbackDelegate.finishPlacementId UTF8String];
		}
		
		bool UAds::getIsUnityAdsReady(const char *placementId)
		{
			if (callbackDelegate.unityAdsReady && callbackDelegate.loadPlacementId == [NSString stringWithUTF8String:placementId])
			{
				callbackDelegate.unityAdsReady = false; // reset property
				return true;
			}
			return false;
		}
		
		bool UAds::getIsUnityAdsDidError()
		{
			if (callbackDelegate.unityAdsDidError)
			{
				callbackDelegate.unityAdsDidError = false; // reset property
				return true;
			}
			return false;
		}
		
		std::string UAds::getUnityAdsError()
		{
			std::string ret = "";
			switch(callbackDelegate.unityAdsError)
			{
				case kUnityAdsErrorNotInitialized: ret = "NotInitialized"; break;
				case kUnityAdsErrorInitializedFailed: ret = "InitializedFailed"; break;
				case kUnityAdsErrorInvalidArgument: ret = "InvalidArgument"; break;
				case kUnityAdsErrorVideoPlayerError: ret = "VideoPlayerError"; break;
				case kUnityAdsErrorInitSanityCheckFail: ret = "InitSanityCheckFail"; break;
				case kUnityAdsErrorAdBlockerDetected: ret = "AdBlockerDetected"; break;
				case kUnityAdsErrorFileIoError: ret = "FileIoError"; break;
				case kUnityAdsErrorDeviceIdError: ret = "DeviceIdError"; break;
				case kUnityAdsErrorShowError: ret = "ShowError"; break;
				case kUnityAdsErrorInternalError: ret = "InternalError"; break;
				default: ret = "NotInitialized";
			}
			callbackDelegate.unityAdsError = kUnityAdsErrorNotInitialized; // reset property
			return ret;
		}
		
		std::string UAds::getUnityAdsErrorMessage()
		{
			std::string ret = [callbackDelegate.unityAdsErrorMessage UTF8String];
			callbackDelegate.unityAdsErrorMessage = @""; // reset property
			return ret;
		}
		
		bool UAds::getIsUnityAdsDidStart(const char *placementId)
		{
			if (callbackDelegate.unityAdsDidStart && [callbackDelegate.startPlacementId isEqualToString:[NSString stringWithUTF8String:placementId]])
			{
				callbackDelegate.unityAdsDidStart = false; // reset property
				return true;
			}
			return false;
		}
		
		bool UAds::getIsUnityAdsDidFinish(const char *placementId)
		{
			if (callbackDelegate.unityAdsDidFinish && [callbackDelegate.finishPlacementId isEqualToString:[NSString stringWithUTF8String:placementId]])
			{
				callbackDelegate.unityAdsDidFinish = false; // reset property
				return true;
			}
			return false;
		}
		
		std::string UAds::getUnityAdsFinishState()
		{
			std::string ret = "";
			switch(callbackDelegate.unityAdsDidFinishState)
			{
				case kUnityAdsFinishStateError: ret = "Error"; break;
				case kUnityAdsFinishStateSkipped: ret = "Skipped"; break;
				case kUnityAdsFinishStateCompleted: ret = "Completed"; break;
				default: ret = "Error";
			}
			callbackDelegate.unityAdsDidFinishState = kUnityAdsFinishStateError; // reset property
			return ret;
		}
		
		UIViewController * UAds::getRootViewController()
		{
			static auto win = Module::getInstance<window::Window>(Module::M_WINDOW);
			
			SDL_Window *window = win-> getWindowObj();
			SDL_SysWMinfo systemWindowInfo;
			SDL_VERSION(&systemWindowInfo.version);
			if (!SDL_GetWindowWMInfo(window, &systemWindowInfo)) {
				printf("Error 0\n");
			}
			UIWindow * appWindow = systemWindowInfo.info.uikit.window;
			UIViewController * rootViewController = appWindow.rootViewController;
			
			return rootViewController;
		}
	}
}
