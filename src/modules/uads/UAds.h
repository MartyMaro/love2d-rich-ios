/**
 * Created by Martin Braun (modiX) for love2d (copied and modified from AdMob port by bio1712)
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef LOVE_UADS_H
#define LOVE_UADS_H

// LOVE
#include "common/config.h"
#include "common/Module.h"
#include "common/StringMap.h"

#include <UnityAds/UnityAds.h>

// stdlib
#include <string>

//SDL
#include <SDL_syswm.h>
#include "window/Window.h"
#include "common/runtime.h"

namespace love
{
	namespace uads
	{		
		class UAds : public Module
		{
			
		public:
			
			virtual ModuleType getModuleType() const { return M_UADS; }
			
			UAds();
			virtual ~UAds() {}
			
			void test(void) const;
			
			bool isReady(const char *placementId);
			
			void show(const char *placementId);
			
			std::string getLatestLoadPlacementId();
			
			std::string getLatestStartPlacementId();
			
			std::string getLatestFinishPlacementId();
			
			bool getIsUnityAdsReady(const char *placementId);
			
			bool getIsUnityAdsDidError();
			
			std::string getUnityAdsError();
			
			std::string getUnityAdsErrorMessage();
			
			bool getIsUnityAdsDidStart(const char *placementId);
			
			bool getIsUnityAdsDidFinish(const char *placementId);
			
			std::string getUnityAdsFinishState();
			
		private:
			
			UIViewController * getRootViewController();
			
		}; // UAds
		
	} // uads
} // love

#endif // LOVE_UADS_H
