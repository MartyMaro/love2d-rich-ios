/**
 * Created by Martin Braun (modiX) for love2d (copied and modified from AdMob port by bio1712)
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

// LOVE
#include "wrap_UAds.h"
#include "sdl/UAds.h"

//lua
extern "C" {
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}

namespace love
{
	namespace uads
	{
		
#define instance() (Module::getInstance<UAds>(Module::M_UADS))
		
		int w_test(lua_State *L __unused)
		{
			//const char *text = luaL_checkstring(L, 1);
			instance()->test();
			return 0;
		}
		
		int w_isReady(lua_State *L)
		{
			const char *placementId = luaL_checkstring(L, 1);
			bool ret = instance()->isReady(placementId);
			luax_pushboolean(L, ret);
			return 1;
		}
		
		int w_show(lua_State *L)
		{
			const char *placementId = luaL_checkstring(L, 1);
			instance()->show(placementId);
			return 0;
		}
		
		int w_getLatestLoadPlacementId(lua_State *L) {
			std::string ret = instance()->getLatestLoadPlacementId();
			luax_pushstring(L, ret);
			return 1;
		}
		
		int w_getLatestStartPlacementId(lua_State *L) {
			std::string ret = instance()->getLatestStartPlacementId();
			luax_pushstring(L, ret);
			return 1;
		}
		
		int w_getLatestFinishPlacementId(lua_State *L) {
			std::string ret = instance()->getLatestFinishPlacementId();
			luax_pushstring(L, ret);
			return 1;
		}
		
		int w_getIsUnityAdsReady(lua_State *L) {
			const char *placementId = luaL_checkstring(L, 1);
			bool ret = instance()->getIsUnityAdsReady(placementId);
			luax_pushboolean(L, ret);
			return 1;
		}
		
		int w_getIsUnityAdsDidError(lua_State *L) {
			bool ret = instance()->getIsUnityAdsDidError();
			luax_pushboolean(L, ret);
			return 1;
		}
		
		int w_getUnityAdsError(lua_State *L) {
			std::string ret = instance()->getUnityAdsError();
			luax_pushstring(L, ret);
			return 1;
		}
		
		int w_getUnityAdsErrorMessage(lua_State *L) {
			std::string ret = instance()->getUnityAdsErrorMessage();
			luax_pushstring(L, ret);
			return 1;
		}
		
		int w_getIsUnityAdsDidStart(lua_State *L) {
			const char *placementId = luaL_checkstring(L, 1);
			bool ret = instance()->getIsUnityAdsDidStart(placementId);
			luax_pushboolean(L, ret);
			return 1;
		}
		
		int w_getIsUnityAdsDidFinish(lua_State *L) {
			const char *placementId = luaL_checkstring(L, 1);
			bool ret = instance()->getIsUnityAdsDidFinish(placementId);
			luax_pushboolean(L, ret);
			return 1;
		}
		
		int w_getUnityAdsFinishState(lua_State *L) {
			std::string ret = instance()->getUnityAdsFinishState();
			luax_pushstring(L, ret);
			return 1;
		}
		
		static const luaL_Reg functions[] =
		{
			{"test", w_test},
			{"isReady", w_isReady},
			{"show", w_show},
			
			// delegate callback poll functions
			{"getLatestLoadPlacementId", w_getLatestLoadPlacementId},
			{"getLatestStartPlacementId", w_getLatestStartPlacementId},
			{"getLatestFinishPlacementId", w_getLatestFinishPlacementId},
			{"getIsUnityAdsReady", w_getIsUnityAdsReady},
			{"getIsUnityAdsDidError", w_getIsUnityAdsDidError},
			{"getUnityAdsError", w_getUnityAdsError},
			{"getUnityAdsErrorMessage", w_getUnityAdsErrorMessage},
			{"getIsUnityAdsDidStart", w_getIsUnityAdsDidStart},
			{"getIsUnityAdsDidFinish", w_getIsUnityAdsDidFinish},
			{"getUnityAdsFinishState", w_getUnityAdsFinishState},
			
			{ 0, 0 }
		};
		
		extern "C" int luaopen_love_uads(lua_State *L)
		{
			UAds *instance = instance();
			if (instance == nullptr)
			{
				instance = new love::uads::sdl::UAds();
			}
			else
			{
				instance->retain();
			}
			
			WrappedModule w;
			w.module = instance;
			w.name = "uads";
			w.type = MODULE_ID;
			w.functions = functions;
			w.types = nullptr;
			
			return luax_register_module(L, w);
		}
		
	} // uads
} // love
