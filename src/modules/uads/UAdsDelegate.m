/**
 * Created by Martin Braun (modiX) for love2d (copied and modified from AdMob port by bio1712)
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#import "UAdsDelegate.h"

@interface UAdsDelegate () <UnityAdsDelegate>

@end

@implementation UAdsDelegate

#pragma mark UnityAdsDelegate implementation

- (void)initProperties {
	printf("Init UAdsDelegate properties\n");
	self.loadPlacementId = @"";
	self.startPlacementId = @"";
	self.finishPlacementId = @"";
	self.unityAdsReady = false;
	self.unityAdsDidError = false;
	self.unityAdsError = (UnityAdsError)kUnityAdsErrorNotInitialized;
	self.unityAdsErrorMessage = @"";
	self.unityAdsDidStart = false;
	self.unityAdsDidFinish = false;
	self.unityAdsDidFinishState = (UnityAdsFinishState)kUnityAdsFinishStateError;
}

- (void)unityAdsReady:(NSString *)placementId {
	NSLog(@"Unity ad %@ ready.", placementId);
	self.loadPlacementId = placementId;
	self.unityAdsReady = true;
}

- (void)unityAdsDidStart:(NSString *)placementId {
	NSLog(@"Unity ad %@ did start.", placementId);
	self.startPlacementId = placementId;
	self.unityAdsDidStart = true;
}

- (void)unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state {
	NSLog(@"Unity ad %@ did finish.", placementId);
	self.finishPlacementId = placementId;
	self.unityAdsDidFinishState = state;
	self.unityAdsDidFinish = true;
}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
	NSLog(@"Unity ad error: %@", message);
	self.unityAdsError = error;
	self.unityAdsErrorMessage = message;
	self.unityAdsDidError = true;
}

@end
