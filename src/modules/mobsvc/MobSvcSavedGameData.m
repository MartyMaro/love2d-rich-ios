/**
 * Created by Martin Braun (Marty) for love2d
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#import "MobSvcSavedGameData.h"
#import <Foundation/Foundation.h>

@interface MobSvcSavedGameData () <NSObject, NSCoding>

@end

@implementation MobSvcSavedGameData

#pragma mark MobSvcSavedGameData implementation

static NSString * const sgDataKey = @"data";

+ (instancetype)sharedGameData {
	static id sharedInstance = nil;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedInstance = [[self alloc] init];
	});
	
	return sharedInstance;
}

- (void)reset
{
	self.data = nil;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.data forKey: sgDataKey];
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)decoder {
	self = [self init];
	if (self) {
		self.data = [decoder decodeObjectForKey:sgDataKey];
	}
	return self;
}

@end
