/**
 * Created by Martin Braun (Marty) for love2d
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>
#include <map>

// LOVE
#include "wrap_MobSvc.h"
#include "sdl/MobSvc.h"

// LUA
extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

// RICH
#include "common/richutil.h"

namespace love
{
	namespace mobsvc
	{
		
#define instance() (Module::getInstance<MobSvc>(Module::M_MOBSVC))
		
		const char *moduleName = "mobsvc";
		long fnc = 0;
		char formatBuffer[1024];
		const char *handlClr = "mobSvcClearCallbacks";
		
		int w_test(lua_State *L __unused)
		{
			instance()->test();
			return 0;
		}
		
		int w_signIn(lua_State *L)
		{
			int na = 1, ao = lua_gettop(L) - na, type, ref;
			if(ao > 0) lua_pop(L, ao); // throw unnecessary arguments out of the stack to prevent issues
			
			type = lua_type(L, -1);
			const char *loveCallback = "mobileServiceSignedIn";
			const char *handlCallback = "mobSvcSignInCallback";
			const char *handlStaticCallback = "mobSvcSignInStaticCallback";
			// the fnc approach fails to set the handlers at 1, some questions in the universe will always struggle humanity ...
			
			if (type == LUA_TFUNCTION) { // arg: callback
				ref = lua_ref(L, true); // pop arg from stack into registry ⁄-1
				luaR_regHandl(L, handlCallback, ref); // love.handlers["mobSvcSignInCallback"] = value(ref) ⁄±0
				lua_unref(L, ref); // remove arg from registry
				ao++; // put nil later on stack ⁄+1
			}
			
			lua_getglobal(L, "love"); // put love on stack ⁄+1
			luaR_gettableval(L, loveCallback); // put love.mobileServiceSignedIn on stack ⁄+1
			ref = lua_ref(L, true); // pop love.mobileServiceSignedIn from stack into registry ⁄-1
			lua_pop(L, 1); // clear stack ⁄-1
			luaR_regHandl(L, handlStaticCallback, ref); // love.handlers["mobSvcSignInStaticCallback"] = value(ref) ⁄±0
			lua_unref(L, ref); // remove love.mobileServiceSignedIn from registry
			
			// prepare callback clear handler and callback fallback handlers
			sprintf(formatBuffer, R"luastring"--(
																					 local handlClr, __NULL__ = love['%s'].__clearCallbacks, love['%s'].__NULL__
																					 love.handlers['%s'] = handlClr
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 --)luastring"--", moduleName, moduleName, handlClr, handlCallback, handlCallback, handlStaticCallback, handlStaticCallback);
			luaR_runCode(L, formatBuffer, "mobsvc.w_signIn");
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			// call await function in new LÖVE thread
			sprintf(formatBuffer, R"luastring"--(
																					 require('love.event') require('love.%s') local e, this = love.event, love['%s']
																					 local playerId = this.signInAwait()
																					 if playerId then e.push("%s", playerId) e.push("%s", playerId) end
																					 e.push("%s", { "%s", "%s" })
																					 --)luastring"--", moduleName, moduleName, handlCallback, handlStaticCallback, handlClr, handlCallback, handlStaticCallback);
			luaR_startThread(L, formatBuffer);
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			for(int i = 0; i < ao; i++) lua_pushnil(L); // reset stack
			return 0;
		}
		
		int w_signInAwait(lua_State *L)
		{
			std::string ret = instance()->signInAwait();
			if(!ret.empty())
			{
				luax_pushstring(L, ret);
				return 1;
			}
			return 0;
		}
		
		int w_isSignedIn(lua_State *L)
		{
			luax_pushboolean(L, instance()->isSignedIn());
			return 1;
		}
		
		int w_downloadSavedGamesMetadata(lua_State *L __unused)
		{
			int na = 1, ao = lua_gettop(L) - na, type, ref;
			if (ao > 0) lua_pop(L, ao); // throw unnecessary arguments out of the stack to prevent issues
			
			type = lua_type(L, -1);
			fnc += 1; // prepare unique module call number as string
			const char *loveCallback = "mobileServiceSavedGamesMetadataDownloaded";
			char handlCallback[64]; sprintf(handlCallback, "mobSvcSavedGamesMetadataDownloadedCallback_%ld", fnc);
			char handlStaticCallback[64]; sprintf(handlStaticCallback, "mobSvcSavedGamesMetadataDownloadedStaticCallback_%ld", fnc);
			
			if (type == LUA_TFUNCTION) { // arg: callback
				ref = lua_ref(L, true); // pop arg from stack into registry ⁄-1
				luaR_regHandl(L, handlCallback, ref); // love.handlers[handlCallback] = value(ref) ⁄±0
				lua_unref(L, ref); // remove arg from registry
				ao++; // put nil later on stack ⁄+1
			}
			
			lua_getglobal(L, "love"); // put love on stack ⁄+1
			luaR_gettableval(L, loveCallback); // put love[loveCallback] on stack ⁄+1
			ref = lua_ref(L, true); // pop love[loveCallback] from stack into registry ⁄-1
			lua_pop(L, 1); // clear stack ⁄-1
			luaR_regHandl(L, handlStaticCallback, ref); // love.handlers[handlStaticCallback] = value(ref) ⁄±0
			lua_unref(L, ref); // remove love.mobileServiceSignedIn from registry
			
			// prepare callback clear handler and callback fallback handlers
			sprintf(formatBuffer, R"luastring"--(
																					 local handlClr, __NULL__ = love['%s'].__clearCallbacks, love['%s'].__NULL__
																					 love.handlers['%s'] = handlClr
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 --)luastring"--", moduleName, moduleName, handlClr, handlCallback, handlCallback, handlStaticCallback, handlStaticCallback);
			luaR_runCode(L, formatBuffer, "mobsvc.w_downloadSavedGamesMetadata");
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			// call await function in new LÖVE thread
			sprintf(formatBuffer, R"luastring"--(
																					 require('love.event') require('love.%s') local e, this = love.event, love['%s']
																					 local savedGamesMetadata = this.downloadSavedGamesMetadataAwait()
																					 if savedGamesMetadata then
																					  					table.sort(savedGamesMetadata, function(a, b)
																											  return a.modifiedTime > b.modifiedTime
																											end)
																					 end
																					 e.push("%s", savedGamesMetadata) e.push("%s", savedGamesMetadata) e.push("%s", { "%s", "%s" })
																					 --)luastring"--", moduleName, moduleName, handlCallback, handlStaticCallback, handlClr, handlCallback, handlStaticCallback);
			luaR_startThread(L, formatBuffer);
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			for (int i = 0; i < ao; i++) lua_pushnil(L); // reset stack
			return 0;
		}
		
		int w_downloadSavedGamesMetadataAwait(lua_State *L)
		{
			std::vector<std::string> ret = instance()->downloadSavedGamesMetadataAwait();
			
			int valsPerItem = 3;
			lua_newtable(L); // ⁄+1
			int top = lua_gettop(L);
			
			std::string value;
			int l = (int)(ret.size() / (u_long)valsPerItem);
			for(int i = 0; i < l; i++)
			{
				int retIOffset = i * valsPerItem;
				lua_pushinteger(L, i + 1);
				lua_newtable(L); // ⁄+1
				int ctop = lua_gettop(L);
				
				lua_pushlstring(L, "name", 4);
				lua_pushlstring(L, ret[retIOffset + 0].c_str(), ret[retIOffset + 0].size());
				lua_settable(L, ctop);
				
				lua_pushlstring(L, "description", 11);
				lua_pushlstring(L, ret[retIOffset + 1].c_str(), ret[retIOffset + 1].size());
				lua_settable(L, ctop);
				
				lua_pushlstring(L, "modifiedTime", 12);
				lua_pushinteger(L, atoi(ret[retIOffset + 2].c_str()));
				lua_settable(L, ctop);
				
				lua_settable(L, top);
			}
			
			return 1;
		}
		
		int w_downloadSavedGameMetadata(lua_State *L __unused)
		{
			int na = 2, ao = lua_gettop(L) - na, type, ref;
			if (ao > 0) lua_pop(L, ao); // throw unnecessary arguments out of the stack to prevent issues
			
			const char *name = luaL_checkstring(L, 1);
			
			type = lua_type(L, -1);
			fnc += 1; // prepare unique module call number as string
			const char *loveCallback = "mobileServiceSavedGameMetadataDownloaded";
			char handlCallback[64]; sprintf(handlCallback, "mobSvcSavedGameMetadataDownloadedCallback_%ld", fnc);
			char handlStaticCallback[64]; sprintf(handlStaticCallback, "mobSvcSavedGameMetadataDownloadedStaticCallback_%ld", fnc);
			
			if (type == LUA_TFUNCTION) { // arg: callback
				ref = lua_ref(L, true); // pop arg from stack into registry ⁄-1
				luaR_regHandl(L, handlCallback, ref); // love.handlers[handlCallback] = value(ref) ⁄±0
				lua_unref(L, ref); // remove arg from registry
				ao++; // put nil later on stack ⁄+1
			}
			
			lua_getglobal(L, "love"); // put love on stack ⁄+1
			luaR_gettableval(L, loveCallback); // put love[loveCallback] on stack ⁄+1
			ref = lua_ref(L, true); // pop love[loveCallback] from stack into registry ⁄-1
			lua_pop(L, 1); // clear stack ⁄-1
			luaR_regHandl(L, handlStaticCallback, ref); // love.handlers[handlStaticCallback] = value(ref) ⁄±0
			lua_unref(L, ref); // remove love.mobileServiceSignedIn from registry
			
			// prepare callback clear handler and callback fallback handlers
			sprintf(formatBuffer, R"luastring"--(
																					 local handlClr, __NULL__ = love['%s'].__clearCallbacks, love['%s'].__NULL__
																					 love.handlers['%s'] = handlClr
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 --)luastring"--", moduleName, moduleName, handlClr, handlCallback, handlCallback, handlStaticCallback, handlStaticCallback);
			luaR_runCode(L, formatBuffer, "mobsvc.w_downloadSavedGameMetadata");
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			// call await function in new LÖVE thread
			sprintf(formatBuffer, R"luastring"--(
																					 require('love.event') require('love.%s') local e, this = love.event, love['%s']
																					 local savedGameMetadata = this.downloadSavedGameMetadataAwait("%s")
																					 e.push("%s", savedGameMetadata) e.push("%s", savedGameMetadata) e.push("%s", { "%s", "%s" })
																					 --)luastring"--", moduleName, moduleName, name, handlCallback, handlStaticCallback, handlClr, handlCallback, handlStaticCallback);
			luaR_startThread(L, formatBuffer);
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			for (int i = 0; i < ao; i++) lua_pushnil(L); // reset stack
			return 0;
		}
		
		int w_downloadSavedGameMetadataAwait(lua_State *L)
		{
			const char *name = luaL_checkstring(L, 1);
			std::vector<std::string> ret = instance()->downloadSavedGameMetadataAwait(name);
			
			if(ret.size() > 2) {
				lua_newtable(L); // ⁄+1
				int top = lua_gettop(L);
				
				lua_pushlstring(L, "name", 4);
				lua_pushlstring(L, ret[0].c_str(), ret[0].size());
				lua_settable(L, top);
				
				lua_pushlstring(L, "description", 11);
				lua_pushlstring(L, ret[1].c_str(), ret[1].size());
				lua_settable(L, top);
				
				lua_pushlstring(L, "modifiedTime", 12);
				lua_pushinteger(L, atoi(ret[2].c_str()));
				lua_settable(L, top);
			}
			else
			{
				lua_pushnil(L);
			}
			
			return 1;
		}
		
		int w_downloadSavedGameData(lua_State *L)
		{
			int na = 2, ao = lua_gettop(L) - na, type, ref; // na = number of arguments
			if(ao > 0) lua_pop(L, ao); // throw unnecessary arguments out of the stack to prevent issues
			
			const char *name = luaL_checkstring(L, 1);
			
			type = lua_type(L, -1);
			fnc += 1; // prepare unique module call number as string
			const char *loveCallback = "mobileServiceSavedGameDataDownloaded";
			char handlCallback[64]; sprintf(handlCallback, "mobSvcSavedGameDataDownloadedCallback_%ld", fnc);
			char handlStaticCallback[64]; sprintf(handlStaticCallback, "mobSvcSavedGameDataDownloadedStaticCallback_%ld", fnc);
			
			if (type == LUA_TFUNCTION) { // arg: callback
				ref = lua_ref(L, true); // pop arg from stack into registry ⁄-1
				luaR_regHandl(L, handlCallback, ref); // love.handlers[handlCallback] = value(ref) ⁄±0
				lua_unref(L, ref); // remove arg from registry
				ao++; // put nil later on stack ⁄+1
			}
			
			lua_getglobal(L, "love"); // put love on stack ⁄+1
			luaR_gettableval(L, loveCallback); // put love[loveCallback] on stack ⁄+1
			ref = lua_ref(L, true); // pop love[loveCallback] from stack into registry ⁄-1
			lua_pop(L, 1); // clear stack ⁄-1
			luaR_regHandl(L, handlStaticCallback, ref); // love.handlers[handlStaticCallback] = value(ref) ⁄±0
			lua_unref(L, ref); // remove handlStaticCallback from registry
			
			// prepare callback clear handler and callback fallback handlers
			sprintf(formatBuffer, R"luastring"--(
																					 local handlClr, __NULL__ = love['%s'].__clearCallbacks, love['%s'].__NULL__
																					 love.handlers['%s'] = handlClr
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 --)luastring"--", moduleName, moduleName, handlClr, handlCallback, handlCallback, handlStaticCallback, handlStaticCallback);
			luaR_runCode(L, formatBuffer, "mobsvc.w_downloadSavedGameData");
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			// call await function in new LÖVE thread
			sprintf(formatBuffer, R"luastring"--(
																					 require('love.event') require('love.%s') local e, this = love.event, love['%s']
																					 local data = this.downloadSavedGameDataAwait("%s")
																					 e.push("%s", data) e.push("%s", "%s", data) e.push("%s", { "%s", "%s" })
																					 --)luastring"--", moduleName, moduleName, name, handlCallback, handlStaticCallback, name, handlClr, handlCallback, handlStaticCallback);
			luaR_startThread(L, formatBuffer);
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			for(int i = 0; i < ao; i++) lua_pushnil(L); // reset stack
			return 0;
		}
		
		int w_downloadSavedGameDataAwait(lua_State *L)
		{
			const char *name = luaL_checkstring(L, 1);
			std::string ret = instance()->downloadSavedGameDataAwait(name);
			lua_pushlstring(L, ret.c_str(), ret.length());
			return 1;
		}
		
		int w_uploadSavedGameData(lua_State *L)
		{
			int na = 4, ao = lua_gettop(L) - na, type, ref; // na = number of arguments
			if(ao > 0) lua_pop(L, ao); // throw unnecessary arguments out of the stack to prevent issues
			
			const char *name = luaL_checkstring(L, 1);
			const char *data = luaL_checkstring(L, 2);
			const char *description = luaL_checkstring(L, 3);
			
			type = lua_type(L, -1);
			fnc += 1; // prepare unique module call number as string
			const char *loveCallback = "mobileServiceSavedGameDataUploaded";
			char handlCallback[64]; sprintf(handlCallback, "mobSvcSavedGameDataUploadedCallback_%ld", fnc);
			char handlStaticCallback[64]; sprintf(handlStaticCallback, "mobSvcSavedGameDataUploadedStaticCallback_%ld", fnc);
			
			if (type == LUA_TFUNCTION) { // arg: callback
				ref = lua_ref(L, true); // pop arg from stack into registry ⁄-1
				luaR_regHandl(L, handlCallback, ref); // love.handlers[handlCallback] = value(ref) ⁄±0
				lua_unref(L, ref); // remove arg from registry
				ao++; // put nil later on stack ⁄+1
			}
			
			lua_getglobal(L, "love"); // put love on stack ⁄+1
			luaR_gettableval(L, loveCallback); // put love[loveCallback] on stack ⁄+1
			ref = lua_ref(L, true); // pop love[loveCallback] from stack into registry ⁄-1
			lua_pop(L, 1); // clear stack ⁄-1
			luaR_regHandl(L, handlStaticCallback, ref); // love.handlers[handlStaticCallback] = value(ref) ⁄±0
			lua_unref(L, ref); // remove handlStaticCallback from registry
			
			// prepare callback clear handler and callback fallback handlers
			sprintf(formatBuffer, R"luastring"--(
																					 local handlClr, __NULL__ = love['%s'].__clearCallbacks, love['%s'].__NULL__
																					 love.handlers['%s'] = handlClr
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 --)luastring"--", moduleName, moduleName, handlClr, handlCallback, handlCallback, handlStaticCallback, handlStaticCallback);
			luaR_runCode(L, formatBuffer, "mobsvc.w_uploadSavedGameData");
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			// call await function in new LÖVE thread
			sprintf(formatBuffer, R"luastring"--(
																					 require('love.event') require('love.%s') local e, this = love.event, love['%s']
																					 local success = this.uploadSavedGameDataAwait("%s", [[%s]], [[%s]])
																					 e.push("%s", success) e.push("%s", "%s", success) e.push("%s", { "%s", "%s" })
																					 --)luastring"--", moduleName, moduleName, name, data, description, handlCallback, handlStaticCallback, name, handlClr, handlCallback, handlStaticCallback);
			luaR_startThread(L, formatBuffer);
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			for(int i = 0; i < ao; i++) lua_pushnil(L); // reset stack
			return 0;
		}
		
		int w_uploadSavedGameDataAwait(lua_State *L)
		{
			int n_args = lua_gettop(L);
			const char *name = luaL_checkstring(L, 1);
			const char *data = luaL_checkstring(L, 2);
			const char *description = "";
			if(n_args >= 3) {
				description = luaL_checkstring(L, 3);
			}
			lua_pushboolean(L, instance()->uploadSavedGameDataAwait(name, data, description));
			return 1;
		}
		
		int w_deleteSavedGame(lua_State *L)
		{
			int na = 2, ao = lua_gettop(L) - na, type, ref; // na = number of arguments
			if(ao > 0) lua_pop(L, ao); // throw unnecessary arguments out of the stack to prevent issues
			
			const char *name = luaL_checkstring(L, 1);
			
			type = lua_type(L, -1);
			fnc += 1; // prepare unique module call number as string
			const char *loveCallback = "mobileServiceSavedGameDeleted";
			char handlCallback[64]; sprintf(handlCallback, "mobSvcSavedGameDeletedCallback_%ld", fnc);
			char handlStaticCallback[64]; sprintf(handlStaticCallback, "mobSvcSavedGameDeletedStaticCallback_%ld", fnc);
			
			if (type == LUA_TFUNCTION) { // arg: callback
				ref = lua_ref(L, true); // pop arg from stack into registry ⁄-1
				luaR_regHandl(L, handlCallback, ref); // love.handlers[handlCallback] = value(ref) ⁄±0
				lua_unref(L, ref); // remove arg from registry
				ao++; // put nil later on stack ⁄+1
			}
			
			lua_getglobal(L, "love"); // put love on stack ⁄+1
			luaR_gettableval(L, loveCallback); // put love[loveCallback] on stack ⁄+1
			ref = lua_ref(L, true); // pop love[loveCallback] from stack into registry ⁄-1
			lua_pop(L, 1); // clear stack ⁄-1
			luaR_regHandl(L, handlStaticCallback, ref); // love.handlers[handlStaticCallback] = value(ref) ⁄±0
			lua_unref(L, ref); // remove handlStaticCallback from registry
			
			// prepare callback clear handler and callback fallback handlers
			sprintf(formatBuffer, R"luastring"--(
																					 local handlClr, __NULL__ = love['%s'].__clearCallbacks, love['%s'].__NULL__
																					 love.handlers['%s'] = handlClr
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 --)luastring"--", moduleName, moduleName, handlClr, handlCallback, handlCallback, handlStaticCallback, handlStaticCallback);
			luaR_runCode(L, formatBuffer, "mobsvc.w_deleteSavedGame");
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			// call await function in new LÖVE thread
			sprintf(formatBuffer, R"luastring"--(
																					 require('love.event') require('love.%s') local e, this = love.event, love['%s']
																					 local success = this.deleteSavedGameAwait("%s")
																					 e.push("%s", success) e.push("%s", "%s", success) e.push("%s", { "%s", "%s" })
																					 --)luastring"--", moduleName, moduleName, name, handlCallback, handlStaticCallback, name, handlClr, handlCallback, handlStaticCallback);
			luaR_startThread(L, formatBuffer);
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			for(int i = 0; i < ao; i++) lua_pushnil(L); // reset stack
			return 0;
		}
		
		int w_deleteSavedGameAwait(lua_State *L)
		{
			const char *name = luaL_checkstring(L, 1);
			lua_pushboolean(L, instance()->deleteSavedGameAwait(name));
			return 1;
		}
		
		int w_showLeaderboards(lua_State *L)
		{
			instance()->showLeaderboards();
			return 0;
		}
		
		int w_showLeaderboard(lua_State *L)
		{
#if defined(LOVE_ANDROID)
			const char *androidLeaderboardId = luaL_checkstring(L, 1);
			instance()->showLeaderboard(androidLeaderboardId);
#elif defined(LOVE_IOS)
			const char *iosLeaderboardId = luaL_checkstring(L, 2);
			instance()->showLeaderboard(iosLeaderboardId);
#endif
			return 0;
		}
		
		int w_uploadLeaderboardScore(lua_State *L)
		{
			int n_args = lua_gettop(L);
			int na = 5, ao = n_args - na, type, ref;
			if(ao > 0) lua_pop(L, ao); // throw unnecessary arguments out of the stack to prevent issues
			
			const char *androidLeaderboardId = luaL_checkstring(L, 1);
			const char *iosLeaderboardId = luaL_checkstring(L, 2);
			long score = luaL_checklong(L, 3);
			const char *format = "none";
			if(n_args >= 4)
			{
				format = luaL_checkstring(L, 4);
			}
			
			type = lua_type(L, -1);
			fnc += 1; // prepare unique module call number as string
			const char *loveCallback = "mobileServiceLeaderboardScoreUploaded";
			char handlCallback[64]; sprintf(handlCallback, "mobSvcUploadLeaderboardScoreCallback_%ld", fnc);
			char handlStaticCallback[64]; sprintf(handlStaticCallback, "mobSvcUploadLeaderboardScoreStaticCallback_%ld", fnc);
			
			if (type == LUA_TFUNCTION) { // arg: callback
				ref = lua_ref(L, true); // pop arg from stack into registry ⁄-1
				luaR_regHandl(L, handlCallback, ref); // love.handlers["mobSvcSignInCallback"] = value(ref) ⁄±0
				lua_unref(L, ref); // remove arg from registry
				ao++; // put nil later on stack ⁄+1
			}
			
			lua_getglobal(L, "love"); // put love on stack ⁄+1
			luaR_gettableval(L, loveCallback); // put love.mobileServiceSignedIn on stack ⁄+1
			ref = lua_ref(L, true); // pop love.mobileServiceSignedIn from stack into registry ⁄-1
			lua_pop(L, 1); // clear stack ⁄-1
			luaR_regHandl(L, handlStaticCallback, ref); // love.handlers["mobSvcSignInStaticCallback"] = value(ref) ⁄±0
			lua_unref(L, ref); // remove love.mobileServiceSignedIn from registry
			
			// prepare callback clear handler and callback fallback handlers
			sprintf(formatBuffer, R"luastring"--(
																					 local handlClr, __NULL__ = love['%s'].__clearCallbacks, love['%s'].__NULL__
																					 love.handlers['%s'] = handlClr
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 --)luastring"--", moduleName, moduleName, handlClr, handlCallback, handlCallback, handlStaticCallback, handlStaticCallback);
			luaR_runCode(L, formatBuffer, "mobsvc.w_signIn");
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			// call await function in new LÖVE thread
			sprintf(formatBuffer, R"luastring"--(
																					 require('love.event') require('love.%s') local e, this = love.event, love['%s']
																					 local success = this.uploadLeaderboardScoreAwait("%s", "%s", %ld, "%s")
																					 e.push("%s", success) e.push("%s", "%s", "%s", success) e.push("%s", { "%s", "%s" })
																					 --)luastring"--", moduleName, moduleName, androidLeaderboardId, iosLeaderboardId, score, format, handlCallback, handlStaticCallback, androidLeaderboardId, iosLeaderboardId, handlClr, handlCallback, handlStaticCallback);
			luaR_startThread(L, formatBuffer);
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			for(int i = 0; i < ao; i++) lua_pushnil(L); // reset stack
			return 0;
		}
		
		int w_uploadLeaderboardScoreAwait(lua_State *L)
		{
			int n_args = lua_gettop(L);
			long score = luaL_checklong(L, 3);
			const char *format = "none";
			if(n_args >= 4) {
				format = luaL_checkstring(L, 4);
			}
			bool ret;
#if defined(LOVE_ANDROID)
			const char *androidLeaderboardId = luaL_checkstring(L, 1);
			ret = instance()->uploadLeaderboardScoreAwait(androidLeaderboardId, score, format);
#elif defined(LOVE_IOS)
			const char *iosLeaderboardId = luaL_checkstring(L, 2);
			ret = instance()->uploadLeaderboardScoreAwait(iosLeaderboardId, score, format);
#endif
			lua_pushboolean(L, ret);
			return 1;
		}
		
		int w_showAchievements(lua_State *L __unused)
		{
			instance()->showAchievements();
			return 0;
		}
		
		int w_incrementAchievementProgress(lua_State *L)
		{
			int na = 5, ao = lua_gettop(L) - na, type, ref;
			if(ao > 0) lua_pop(L, ao); // throw unnecessary arguments out of the stack to prevent issues
			
			const char *androidAchievementId = luaL_checkstring(L, 1);
			const char *iosAchievementId = luaL_checkstring(L, 2);
			int steps = luaL_checkint(L, 3);
			int maxSteps = luaL_checkint(L, 4);
			
			type = lua_type(L, -1);
			fnc += 1; // prepare unique module call number as string
			const char *loveCallback = "mobileServiceAchievementProgressIncremented";
			char handlCallback[64]; sprintf(handlCallback, "mobSvcIncrementAchievementProgressCallback_%ld", fnc);
			char handlStaticCallback[64]; sprintf(handlStaticCallback, "mobSvcIncrementAchievementProgressStaticCallback_%ld", fnc);
			
			if (type == LUA_TFUNCTION) { // arg: callback
				ref = lua_ref(L, true); // pop arg from stack into registry ⁄-1
				luaR_regHandl(L, handlCallback, ref); // love.handlers["mobSvcSignInCallback"] = value(ref) ⁄±0
				lua_unref(L, ref); // remove arg from registry
				ao++; // put nil later on stack ⁄+1
			}
			
			lua_getglobal(L, "love"); // put love on stack ⁄+1
			luaR_gettableval(L, loveCallback); // put love.mobileServiceSignedIn on stack ⁄+1
			ref = lua_ref(L, true); // pop love.mobileServiceSignedIn from stack into registry ⁄-1
			lua_pop(L, 1); // clear stack ⁄-1
			luaR_regHandl(L, handlStaticCallback, ref); // love.handlers["mobSvcSignInStaticCallback"] = value(ref) ⁄±0
			lua_unref(L, ref); // remove love.mobileServiceSignedIn from registry
			
			// prepare callback clear handler and callback fallback handlers
			sprintf(formatBuffer, R"luastring"--(
																					 local handlClr, __NULL__ = love['%s'].__clearCallbacks, love['%s'].__NULL__
																					 love.handlers['%s'] = handlClr
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 if not rawget(love.handlers, '%s') then love.handlers['%s'] = __NULL__ end
																					 --)luastring"--", moduleName, moduleName, handlClr, handlCallback, handlCallback, handlStaticCallback, handlStaticCallback);
			luaR_runCode(L, formatBuffer, "mobsvc.w_signIn");
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			// call await function in new LÖVE thread
			sprintf(formatBuffer, R"luastring"--(
																					 require('love.event') require('love.%s') local e, this = love.event, love['%s']
																					 local success, unlocked = this.incrementAchievementProgressAwait("%s", "%s", %d, %d)
																					 e.push("%s", success, unlocked) e.push("%s", "%s", "%s", success, unlocked) e.push("%s", { "%s", "%s" })
																					 --)luastring"--", moduleName, moduleName, androidAchievementId, iosAchievementId, steps, maxSteps, handlCallback, handlStaticCallback, androidAchievementId, iosAchievementId, handlClr, handlCallback, handlStaticCallback);
			luaR_startThread(L, formatBuffer);
			memset(formatBuffer, 0, sizeof formatBuffer);
			
			for(int i = 0; i < ao; i++) lua_pushnil(L); // reset stack
			return 0;
		}
		
		int w_incrementAchievementProgressAwait(lua_State *L)
		{
			int n_args = lua_gettop(L);
			long steps = luaL_checklong(L, 3);
			long maxSteps = luaL_checklong(L, 4);
			std::vector<bool> ret;
#if defined(LOVE_ANDROID)
			const char *androidAchievementId = luaL_checkstring(L, 1);
			ret = instance()->incrementAchievementProgressAwait(androidAchievementId, steps, maxSteps);
#elif defined(LOVE_IOS)
			const char *iosAchievementId = luaL_checkstring(L, 2);
			ret = instance()->incrementAchievementProgressAwait(iosAchievementId, steps, maxSteps);
#endif
			lua_pushboolean(L, ret[0]);
			lua_pushboolean(L, ret[1]);
			return 2;
		}
		
		static const luaL_Reg functions[] =
		{
			{ "test", w_test },
			{ "signIn", w_signIn }, { "signInAwait", w_signInAwait },
			{ "isSignedIn", w_isSignedIn },
			{ "downloadSavedGamesMetadata", w_downloadSavedGamesMetadata }, { "downloadSavedGamesMetadataAwait", w_downloadSavedGamesMetadataAwait },
			{ "downloadSavedGameMetadata", w_downloadSavedGameMetadata }, { "downloadSavedGameMetadataAwait", w_downloadSavedGameMetadataAwait },
			{ "downloadSavedGameData", w_downloadSavedGameData }, { "downloadSavedGameDataAwait", w_downloadSavedGameDataAwait },
			{ "uploadSavedGameData", w_uploadSavedGameData }, { "uploadSavedGameDataAwait", w_uploadSavedGameDataAwait },
			{ "deleteSavedGame", w_deleteSavedGame }, { "deleteSavedGameAwait", w_deleteSavedGameAwait },
			{ "showLeaderboards", w_showLeaderboards }, { "showLeaderboard", w_showLeaderboard },
			{ "uploadLeaderboardScore", w_uploadLeaderboardScore }, { "uploadLeaderboardScoreAwait", w_uploadLeaderboardScoreAwait },
			{ "showAchievements", w_showAchievements },
			{ "incrementAchievementProgress", w_incrementAchievementProgress }, { "incrementAchievementProgressAwait", w_incrementAchievementProgressAwait },
			{ 0, 0 }
		};
		
		extern "C" int luaopen_love_mobsvc(lua_State *L)
		{
			MobSvc *instance = instance();
			if (instance == nullptr) instance = new love::mobsvc::sdl::MobSvc(); else instance->retain();
			
			WrappedModule w;
			w.module = instance;
			w.name = moduleName;
			w.type = MODULE_ID;
			w.functions = functions;
			w.types = nullptr;
			
			int ret = luax_register_module(L, w);
			
			luaR_runCode(L, R"luastring"--(
																		 if not love.threaderror then love.threaderror = function(t,m) love.errhand(m ..' (' .. tostring(t) .. ')') end end
																		 local this = love.mobsvc
																		 this.__NULL__ = function() end
																		 this.__clearCallbacks = function(c) for _,n in ipairs(c) do love.handlers[n] = nil end end
																		 this.newLeaderboard = function(a,i,f) return { _androidId = a, _iosId = i, _format = f,
																			 show = function(self) this.showLeaderboard(self._androidId, self._iosId) end,
																			 uploadScore = function(self, score) this.uploadLeaderboardScore(self._androidId, self._iosId, score, self._format) end,
																		 } end
																		 this.newAchievement = function(a,i,ms) return { _androidId = a, _iosId = i, _maxSteps = ms,
																			 incrementProgress = function(self, steps) this.incrementAchievementProgress(self._androidId, self._iosId, steps, self._maxSteps) end,
																		 } end
																		 --)luastring"--", moduleName);
			
			return ret;
		}
		
	} // mobsvc
} // love
