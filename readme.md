# RichLÖVE Mobile (iOS) 0.10.2 - AdMob+UnityAds+GameCenter
[//]: # (Soon: AdMob+UnityAds+InAppPurchases+GameCenter)

LÖVE is an *awesome* framework you can use to make 2D games in Lua. It's free, open-source, and works on Windows, Mac OS X, Linux, Android, and iOS.

RichLÖVE is an *awesome* fork that allows your game to have features that are usual for mobile platforms, such as displaying ads.

LÖVE Documentation
------------------

Use our [wiki][wiki] for documentation.
If you need further help, feel free to ask on our [forums][forums], and last but not least there's the irc channel [#love on OFTC][irc] and Discord server [LÖVE][discord].

Rich Modules
------------

- `[love.ads]--` **AdMob** (created by bio1712) *- Displays banner, interstitial and reward video ads using the native Ad-API of Google, admob.google.com)*
- `[love.uads]-` **UnityAds** *- Displays video and reward video ads using the native Ad-API of Unity3D, operate.dashboard.unity3d.com)*
- `[love.mobsvc]-` **MobileServices** *- Provides a common API for Saved Games, Leaderboards and Achievements using Game Center, developer.apple.com/game-center)*

Configuration
-------------

### AdMob ###

Configure [`src/modules/ads/Ads.mm`](src/modules/ads/Ads.mm) to match your iOS AdMob APP-ID and, additionally for testing, your iOS AdMob TEST-DEVICE-ID. If you don't have any TEST-DEVICE-ID, only use [TestAds][admobtestids] within your game. 

When debugging your game, you can optain the TEST-DEVICE-ID in the logs. After adding your TEST-DEVICE-ID, you can use real ads in development on this device.

Be careful to not display real ads without a correct TEST-DEVICE-ID, as even simple ad impressions can cause a device ban.

### UnityAds ###

Configure [`src/modules/uads/config_UAds.h`](src/modules/uads/config_UAds.h) to match your iOS UnityAds GAME-ID and, additionally for testing, the TEST-MODE flag. Make sure that your device is added as test device or the global client testing flag is enabled on the UnityAds dashboard before disabling the TEST-MODE flag in development.

### MobileServices ###

If you want to use the Saved Games feature, please [setup an iCloud container for your game](https://developer.apple.com/library/archive/documentation/DataManagement/Conceptual/CloudKitQuickStart/EnablingiCloudandConfiguringCloudKit/EnablingiCloudandConfiguringCloudKit.html).

Compilation
-----------

Use the Xcode project found at [`platform/xcode/love.xcodeproj`](platform/xcode/love.xcodeproj) to build the `love-ios` target.

See [`readme-iOS.rtf`](readme-iOS.rtf) for more information. Required libraries are downloaded and added, already.

API
---

For the API documentation, please visit the [forum thread of RichLÖVE][forumthread].

[site]: http://love2d.org
[wiki]: http://love2d.org/wiki
[forums]: http://love2d.org/forums
[irc]: irc://irc.oftc.net/love
[discord]: https://discord.gg/H3zQShj
[admobtestids]: https://developers.google.com/admob/ios/test-ads
[forumthread]: https://love2d.org/forums/viewtopic.php?f=5&t=85468&p=221993#p221993